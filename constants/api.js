(function() {
    'use strict';

    angular
        .module('wooza')
        .constant('API', {
            URL : 'http://private-59658d-celulardireto2017.apiary-mock.com',
            GET_PLATAFORMAS : '/plataformas',
            GET_PLANOS : '/planos/$$'
        });

})();