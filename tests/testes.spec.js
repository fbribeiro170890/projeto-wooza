
var clienteEmBranco = {
  nome: null,
  email: null,
  nascimento: null,
  cpf: null,
  telefone: null,
  plataforma: null,
  plano: null
};

var clienteAlgumasInformacoesEmBranco = {
  nome: "Fabio Balbino Ribeiro",
  email: null,
  nascimento: null,
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: null,
  plano: null
};

var clienteEmailIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro',
  nascimento: '17/08/1990',
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

var clienteNascimentoIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/',
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

var clienteCPFIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/1990',
  cpf: "1111111111",
  telefone: "11111111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

var clienteTelefoneIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/1990',
  cpf: "11111111111",
  telefone: "11111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

var clientePlataformaIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/1990',
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: null,
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

var clientePlanoIncorreto = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/1990',
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: null
};

var clienteOk = {
  nome: "Fabio Balbino Ribeiro",
  email: 'fbribeiro@live.com',
  nascimento: '17/08/1990',
  cpf: "11111111111",
  telefone: "11111111111",
  plataforma: {
    sku: "TBT01",
    nome: "Tablet",
    descricao: "Chip para navegar à vontade"
  },
  plano: {
    sku: "TI00001NA_NOVA_LINHA",
    franquia: "1GB",
    valor: "21,50",
    ativo: true
  }
};

function verifyFormulario(cliente) {
  for (var i in cliente) if (!cliente[i]) return false;
  if (cliente.nome.length < 10 || cliente.email.length < 10 || cliente.nascimento.length != 10
      || cliente.cpf.length < 11 || cliente.telefone.length < 11) return false; 
  return true;
}

describe('Verificação de preenchimento do formulário', function() {
    it('deu ok em com todos os campos em branco', function() {
      expect(verifyFormulario(clienteEmBranco)).toEqual(false);
    });
    it('deu ok com alguns campos em branco', function() {
      expect(verifyFormulario(clienteAlgumasInformacoesEmBranco)).toEqual(false);
    });
    it('deu ok com email incorreto', function() {
      expect(verifyFormulario(clienteEmailIncorreto)).toEqual(false);
    });
    it('deu ok com nascimento incorreto', function() {
      expect(verifyFormulario(clienteNascimentoIncorreto)).toEqual(false);
    });
    it('deu ok com CPF incorreto', function() {
      expect(verifyFormulario(clienteCPFIncorreto)).toEqual(false);
    });
    it('deu ok com telefone incorreto', function() {
      expect(verifyFormulario(clienteTelefoneIncorreto)).toEqual(false);
    });
    it('deu ok com plataforma incorreto', function() {
      expect(verifyFormulario(clientePlataformaIncorreto)).toEqual(false);
    });
    it('deu ok com plano incorreto', function() {
      expect(verifyFormulario(clientePlanoIncorreto)).toEqual(false);
    });
    it('cliente ok', function() {
      expect(verifyFormulario(clienteOk)).toEqual(true);
    });
  });