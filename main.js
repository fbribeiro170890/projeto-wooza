(function() {
    'use strict';

    angular
        .module('wooza', [])
        .controller('MainController', MainController)
        .component('plataformas', {
            templateUrl: 'components/plataformas.html',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .component('planos', {
            templateUrl: 'components/planos.html',
            controller: 'MainController',
            controllerAs: 'vm'
        })
        .component('formulario', {
            templateUrl: 'components/formulario.html',
            controller: 'MainController',
            controllerAs: 'vm'
        });

    MainController.$inject = [
        'MainService',
        'CelularDiretoService',
        '$timeout',
        '$rootScope'
    ];
    function MainController(
        MainService,
        CelularDiretoService,
        $timeout,
        $rootScope
    ) {
        var vm = this;
        vm.getPlanos = getPlanos;
        vm.getFromPlataformas = getFromPlataformas;
        vm.getFromPlanos = getFromPlanos;
        vm.getFromFormulario = getFromFormulario;
        vm.setPlano = setPlano;
        vm.closeForm = closeForm;
        vm.setFormulario = setFormulario;
        vm.cliente = {
            nome: null,
            email: null,
            nascimento: null,
            cpf: null,
            telefone: null,
            plataforma: null,
            plano: null
        };

        init();

        function init() {
            getPlataformas();
        }

        /**
         * recupera as plataformas
         */
        function getPlataformas(){
            CelularDiretoService.getPlataformas().then(function (res){
                $rootScope.info.plataformas = res.plataformas;
            }).catch(function(){
                console.error('Não conseguiu retornar o get plataformas.');
            });
        }

        /**
         * Recupera os planos da plataforma selecionada
         * @param {*} sku 
         */
        function getPlanos(index) {
            $rootScope.cliente.plataforma = $rootScope.info.plataformas[index];
            CelularDiretoService.getPlanos($rootScope.info.plataformas[index].sku).then(function (res){
                $rootScope.info.planos = res.planos;
                scrollToPlano();
            }).catch(function(){
                console.error('Não conseguiu retornar o get plataformas.');
            });
        }

        /**
         * define o plano selecionado
         * @param {*} index 
         */
        function setPlano(index) {
            $rootScope.cliente.plano = $rootScope.info.planos[index];
            $rootScope.info.selecionado = true;
            scrollToTop();
        }

        /**
         * fecha o formulário
         */
        function closeForm() {
            $rootScope.info.selecionado = false;
            scrollToTop();
        }

        /**
         * scrolla para o card de planos
         */
        function scrollToPlano(){
            $timeout(function (){ document.getElementsByClassName("container-scroll")[0].scrollTo(0, 400); }, 500);
        }

        /**
         * scrolla para cima
         */
        function scrollToTop(){
            $timeout(function (){ document.getElementsByClassName("container-scroll")[0].scrollTo(0, 0); }, 500);
        }

        /**
         * verifica as informações inseridas e envia o formulário
         */
        function setFormulario() {
            vm.cliente.plano = $rootScope.cliente.plano;
            vm.cliente.plataforma = $rootScope.cliente.plataforma;
            if (verifyFormulario(vm.cliente)) {
                $rootScope.cliente = vm.cliente;
                $rootScope.info.selecionado = false;
                $rootScope.info.plataformas = null;
                $rootScope.info.planos = null;
                console.log($rootScope.cliente);
            }
        }

        function verifyFormulario(cliente) {
            for (var i in cliente) if (!cliente[i]) return false;
            if (cliente.nome.length < 10 || cliente.email.length < 10 || cliente.nascimento.length != 10
                || cliente.cpf.length < 11 || cliente.telefone.length < 11) return false; 
            return true;
        }

        /**
         * faz a ponte com o service para pegar as plataformas
         */
        function getFromPlataformas() {
            return !$rootScope.info.selecionado && $rootScope.info.plataformas;
        }

        /**
         * faz a ponte com service para pegar os planos
         */
        function getFromPlanos() {
            return !$rootScope.info.selecionado && $rootScope.info.planos;
        }

        /**
         * verifica se o formulário deve ser exibido;
         */
        function getFromFormulario() {
            return $rootScope.info.selecionado;
        }
    }
})();