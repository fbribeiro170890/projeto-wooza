! function() {
    "use strict";

    angular
        .module("wooza")
        .service("CelularDiretoService", CelularDiretoService), 
    
    CelularDiretoService.$inject = [
        "HTTPService",
        "API",
        "$rootScope"
    ]
    
    function CelularDiretoService(
        HTTPService,
        API,
        $rootScope
    ) {
    
        var vm = this;
        vm.getPlataformas = getPlataformas;
        vm.getPlanos = getPlanos;
        
        $rootScope.cliente = {
            nome: null,
            email: null,
            nascimento: null,
            cpf: null,
            telefone: null,
            plataforma: null,
            plano: null
        };
        $rootScope.info = {
            plataformas: null,
            planos: null,
            selecionado: false
        }

        /**
         * Recupera as plataformas
         */
        function getPlataformas(){
            var url = API.URL + API.GET_PLATAFORMAS;
            return HTTPService.get(url);
        }

        /**
         * Recupera os planos
         * @param {*} sku 
         */
        function getPlanos(sku){
            var url = API.URL + API.GET_PLANOS.replace("$$", sku);
            return HTTPService.get(url);
        }
    }
}();