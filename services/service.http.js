(function () {

  'use strict';

  angular
    .module('wooza')
    .service('HTTPService', HTTPService);

  HTTPService.$inject = [
    '$http'
  ];

  function HTTPService(
    $http
  ) {

    /* fields */
    var vm = this;
    vm.get = get;
    vm.post = post;

    /* methods */

    /**
     *
     *
     * @param {any} path
     * @param {any} params
     * @param {number|Promise} timeout
     * @returns
     */
    function get(path, params, timeout) {
      timeout = timeout || null;
      return $http.get(path, { params: params, timeout: timeout }).then(_complete).catch(_error);
    }

    /**
     *
     *
     * @param {any} path
     * @param {any} params
     * @param {number|Promise} timeout
     * @returns
     */
    function post(path, params, timeout) {
      timeout = timeout || null;
      return $http.post(path, params, { timeout: timeout }).then(_complete).catch(_error);
    }

    /* private */

    /**
     *
     *
     * @param {any} data
     * @param {any} status
     * @param {any} headers
     * @param {any} config
     * @returns
     */
    function _complete(data, status, headers, config) {
      return data.data;
    }

    /**
     *
     *
     * @param {any} message
     */
    function _error(message) {
      throw new Error('XHR Failed: ' + message);
    };

  }
})();